local MakePlayerCharacter = require "prefabs/player_common"

local assets = 
{
		Asset("SCRIPT", "scripts/prefabs/player_common.lua"),
		Asset("SCRIPT", "scripts/prefabs/skilltree_weerclops.lua"),

		Asset("ANIM", "anim/weerclops.zip"),
		Asset("ANIM", "anim/ghost_weerclops_build.zip" ),
		Asset("ANIM", "anim/player_idles_weerclops.zip"),
}

local start_inv =
{
	default =
	{},
}
for k, v in pairs(TUNING.GAMEMODE_STARTING_ITEMS) do
	start_inv[string.lower(k)] = v.WEERCLOPS
end
local prefabs = FlattenTree(start_inv, true)
--------------------------------------------------------------------------
local function IsWinter(season)
	return season == "winter"
end

local function OnWinter(inst, season)
	if IsWinter(season) then
		inst:AddTag("winterclops")
		if inst.components.skilltreeupdater ~= nil and not inst.components.skilltreeupdater:IsActivated("weerclops_coldness_1") then
			SendRPCToClient(CLIENT_RPC.UpdateAccomplishment, inst.userid, "weerclops_winter")
		end
	else
		inst:RemoveTag("winterclops")
	end
end

local function DoSpawnIceSpike(inst, x, z)
	inst.spikes = SpawnPrefab("icespike_fx_"..tostring(math.random(1, 4)))
	if IsWinter(TheWorld.state.season) then
		inst.spikes.Transform:SetScale(1.5, 1.5, 1.5)
	end
	inst.spikes.Transform:SetPosition(x, 0, z)
end

local function SpawnIceFx(inst, target)
	if target == nil or not target:IsValid() then
		return
	end

	local x, y, z = target.Transform:GetWorldPosition()
	if not IsWinter(TheWorld.state.season) then
		local numFX = math.random(2, 7)
		for i = 1, numFX do
			inst:DoTaskInTime(math.random() * .25, DoSpawnIceSpike, x + math.random(-1.25,.75), z + math.random(-1.25,.75))
		end
	end
	if IsWinter(TheWorld.state.season) then
		local numFX = math.random(7, 10)
		for i = 1, numFX do
				inst:DoTaskInTime(math.random() * .25, DoSpawnIceSpike, x + math.random(-2,2), z + math.random(-2,2))
		end
	end
end
--	For coldness
local function weerclops_onhit(inst, data)
	local target = data.target
	if target ~= nil and inst:HasTag("frozenclaw") then

	if data.weapon ~= nil then
		if data.projectile == nil then
			if data.weapon.components.projectile ~= nil then
				return
			elseif data.weapon.components.complexprojectile ~= nil then
				return
			elseif data.weapon.components.weapon:CanRangedAttack() then
				return
			elseif data.weapon.components.lighter ~= nil then
				return
			end
		end
	end

	if inst:HasTag("winterclops") or IsWinter(TheWorld.state.season) then
		if not (target.components.health ~= nil and target.components.health:IsDead()) then
			if target.components.freezable ~= nil then
				target.components.freezable:AddColdness(inst.freezepower*2 or 0.6, inst.freezetimer*2)
				if inst:HasTag("noisysmasher") then
					local icefx = SpawnPrefab("mining_ice_fx")
					icefx.Transform:SetPosition(target.Transform:GetWorldPosition())
				end
			end
		end
	else
		if not (target.components.health ~= nil and target.components.health:IsDead()) then
			if target.components.freezable ~= nil then
				target.components.freezable:AddColdness(inst.freezepower or 0.3, inst.freezetimer)
			end
		end
	end

	if inst.components.skilltreeupdater and inst.components.skilltreeupdater:IsActivated("weerclops_slowing_1") then
		local debuffkey = inst.prefab
		local slowingskill2 = inst.components.skilltreeupdater:IsActivated("weerclops_slowing_2")

		if target:IsValid() and target.components.locomotor ~= nil and target.components.freezable ~= nil and target.components.freezable:IsFrozen() then
			if target._weerclops_speedmulttask ~= nil then
				target._weerclops_speedmulttask:Cancel()
			end
			target._weerclops_speedmulttask = target:DoTaskInTime(slowingskill2 and 30 or 18, function(i) i.components.locomotor:RemoveExternalSpeedMultiplier(i, debuffkey) i._weerclops_speedmulttask = nil end)

			target.components.locomotor:SetExternalSpeedMultiplier(target, debuffkey, 2/3)
		end
	end

	end
end
--	For ice spikes
local function onattack(inst, data)
	if data.weapon ~= nil then
		if data.weapon.components.projectile ~= nil then
				return
		elseif data.weapon.components.complexprojectile ~= nil then
				return
			elseif data.weapon.components.weapon:CanRangedAttack() then
				return
			elseif data.weapon.components.lighter ~= nil then
				return
		end
	end

	if inst:HasTag("frozenclaw") then

	local chance = math.random()
	local target = data.target
	local x, y, z = target.Transform:GetWorldPosition()
	local colduaraskill2 = inst.components.skilltreeupdater and inst.components.skilltreeupdater:IsActivated("weerclops_cold_aura_2")

	if IsWinter(TheWorld.state.season) then
		local chance = chance + .2
	end

	if chance >= .6 then
		if inst:HasTag("noisysmasher") then
			SpawnIceFx(inst, target)
		end
		if IsWinter(TheWorld.state.season) or (inst.iceauraenabled == true and colduaraskill2) then
			local ents = TheSim:FindEntities(x, y, z, TUNING.WEERCLOPS_ICE_RANGE, nil)
			for k,v in pairs(ents) do
				if v and v:IsValid() and v.components.health ~= nil and not v.components.health:IsDead() then
					if not (v:HasTag("player") or v:HasTag("INLIMBO") or v:HasTag("companion")) then
						if v.components.freezable ~= nil then
							v.components.freezable:AddColdness(inst.freezepower/2 or 0.15)
						end
		    		end
	        	end
		    end
		end
	end

	end
end

local function OnAttacked(inst, data)
	local target = data.attacker
	if inst.components.skilltreeupdater and inst.components.skilltreeupdater:IsActivated("weerclops_coldness_3") then
		if target ~= nil and target:IsValid() and target.components.freezable ~= nil then
			target.components.freezable:AddColdness(inst.freezepower*2, 1, true)
		end
	end
end

local function CustomCombatDamage(inst, target, weapon, multiplier, mount)
	local shatterskill1 = inst.components.skilltreeupdater and inst.components.skilltreeupdater:IsActivated("weerclops_shatter_1")
	if shatterskill1 then
		if target ~= nil and target.components.freezable ~= nil and target.components.freezable:IsFrozen() then
			return 2
		end
	end
end
--------------------------------------------------------------------------
local function OnTimerDone(inst, data)
	if data.name == "stage1" then
		inst.components.talker:Say(GetString(inst, "ANNOUNCE_CRAVE_1"))
		inst.components.sanity.dapperness = -TUNING.DAPPERNESS_TINY
		inst.components.timer:StartTimer("stage2", TUNING.TOTAL_DAY_TIME*1) --stage2
		inst.stage = 1
	end
	if data.name == "stage2" then
		inst.components.talker:Say(GetString(inst, "ANNOUNCE_CRAVE_2"))
		inst.components.sanity.dapperness = -TUNING.DAPPERNESS_LARGE/2
		inst.components.timer:StartTimer("stage3", TUNING.TOTAL_DAY_TIME*2) --stage3
		inst.stage = 2
	end
	if data.name == "stage3" then
		inst.components.talker:Say(GetString(inst, "ANNOUNCE_CRAVE_3"))
		inst.components.sanity.dapperness = -TUNING.DAPPERNESS_LARGE
		inst.components.timer:StartTimer("stage4", TUNING.TOTAL_DAY_TIME*3) --stage4
		inst.stage = 3
	end
	if data.name == "stage4" then --stage3 timer ends in final stage
		inst.components.talker:Say(GetString(inst, "ANNOUNCE_CRAVE_4"))
		inst.components.sanity.dapperness = -TUNING.DAPPERNESS_LARGE*3 --no more timers and max sanity loss per second happens here
		inst.stage = 4
	end
end

local function StopTimers(inst, data)
	if inst.components.timer:TimerExists("stage1") then
		inst.components.timer:StopTimer("stage1")
	end
	if inst.components.timer:TimerExists("stage2") then
		inst.components.timer:StopTimer("stage2")
	end
	if inst.components.timer:TimerExists("stage3") then
		inst.components.timer:StopTimer("stage3")
	end
	if inst.components.timer:TimerExists("stage4") then
		inst.components.timer:StopTimer("stage4")
	end
	inst.stage = 0
	inst.components.sanity.dapperness = 0
end
--------------------------------------------------------------------------
local function GetPointSpecialActions(inst, pos, useitem, right)
    if right and inst.components.playercontroller.isclientcontrollerattached then -- This is for Controllers Only
		if inst.components.skilltreeupdater and inst.components.skilltreeupdater:IsActivated("weerclops_cryofreeze_1") then
			return { ACTIONS.WEERCLOPS_FREEZE }
		end
	end
    return {}
end

local function OnSetOwner(inst)
    if inst.components.playeractionpicker ~= nil then
        inst.components.playeractionpicker.pointspecialactionsfn = GetPointSpecialActions
    end
end
--------------------------------------------------------------------------
local function SkillBasedPreserverRateFn(inst, item)
	local fridge_1_skill = inst.components.skilltreeupdater:IsActivated("weerclops_fridge_1")
	local fridge_2_skill = inst.components.skilltreeupdater:IsActivated("weerclops_fridge_2")
	local fridge_3_skill = inst.components.skilltreeupdater:IsActivated("weerclops_fridge_3")

    return (fridge_3_skill and 0.4)
        or (fridge_2_skill and 0.6)
        or (fridge_1_skill and 0.8)
        or 1
end
--------------------------------------------------------------------------
local function onbecamedeerclops(inst)
	StopTimers(inst)
	inst.components.timer:StartTimer("stage1", TUNING.TOTAL_DAY_TIME * TUNING.WEERCLOPS_SANITYSTART)--stage1
end

local function OnRespawnedFromGhost(inst)
	onbecamedeerclops(inst)
	if inst.components.slipperyfeet ~= nil then
		inst.components.slipperyfeet.threshold = 2400
	end
end

local function DoShatter(inst, owner)
	local shatterskill3 = owner.components.skilltreeupdater and owner.components.skilltreeupdater:IsActivated("weerclops_shatter_3")
	local shatter = SpawnPrefab("weerclops_shatterfx")
	shatter.Transform:SetPosition(inst.Transform:GetWorldPosition())
	shatter:SetFXOwner(owner)
	shatter.damage = shatterskill3 and 150 or 75
	shatter.range = shatterskill3 and 2 or 1
end

local function onkilled(inst, data)
	if data ~= nil and data.victim ~= nil then
		if data.victim.prefab == "deerclops" then
			if inst.components.skilltreeupdater ~= nil and not inst.components.skilltreeupdater:IsActivated("weerclops_shatter_1") then
				SendRPCToClient(CLIENT_RPC.UpdateAccomplishment, inst.userid, "weerclops_superior_deerclops")
			end
		end

		if data.victim.components.freezable ~= nil and data.victim.components.freezable:IsFrozen() then
			if inst.components.skilltreeupdater ~= nil and inst.components.skilltreeupdater:IsActivated("weerclops_shatter_2") then
				DoShatter(data.victim, inst)
			end
		end
	end
end

local function OnTemperatureDelta(inst, data)
	if inst.hasiceaura == true then
		local break_threshold = 20
		if (data.last < break_threshold and data.new <= break_threshold) then
			if inst.iceauraenabled == false then
				inst.iceauraenabled = true
				inst.components.frostybreather:ForceBreathOn()
				inst.components.heater:SetThermics(false, true)
				local icefx = SpawnPrefab("deer_ice_flakes")
				icefx.Transform:SetScale(.5, .5, .5)
				icefx.AnimState:PlayAnimation("idle")
				icefx.entity:SetParent(inst.entity)
				icefx:ListenForEvent("animover", icefx.Remove)
			end
		else
			if inst.iceauraenabled == true then
				inst.iceauraenabled = false
				inst.components.frostybreather:ForceBreathOff()
			end
		end
	end
end

local function structuresated(inst, data)
	if data.target:HasTag("wall") or data.target:HasTag("structure") then
		onbecamedeerclops(inst)
		inst.components.talker:Say(GetString(inst, "ANNOUNCE_STRUCTURE_DESTROYED"))
	end

	if data.target:HasTag("boulder") or data.target:HasTag("buried") or data.target:HasTag("renewable") or data.target:HasTag("carnivalgame_part") or data.target:HasTag("carnivaldecor") or data.target:HasTag("little_smashable") then 
		inst.components.sanity:DoDelta(TUNING.WEERCLOPS_STRUCTURE_SANITY/5)--1
	elseif data.target:HasTag("sign") or data.target:HasTag("stump") then
		inst.components.sanity:DoDelta(TUNING.WEERCLOPS_STRUCTURE_SANITY/10)--0.5 --Avoid 1 recipe cheese
	elseif data.target:HasTag("door") or data.target:HasTag("chest") or data.target:HasTag("object") or data.target:HasTag("campfire") then
		inst.components.sanity:DoDelta(TUNING.WEERCLOPS_STRUCTURE_SANITY/2)--2.5(3)
	elseif data.target:HasTag("wardrobe") or data.target:HasTag("prototyper") or data.target:HasTag("statue") then
		inst.components.sanity:DoDelta(TUNING.WEERCLOPS_STRUCTURE_SANITY*1.2)--6
	elseif data.target:HasTag("larger_smashable") or data.target:HasTag("altar") or data.target:HasTag("shelter") then
		inst.components.sanity:DoDelta(TUNING.WEERCLOPS_STRUCTURE_SANITY*1.5)--7.5(8)
	else
		inst.components.sanity:DoDelta(TUNING.WEERCLOPS_STRUCTURE_SANITY)
	end

	if inst.components.skilltreeupdater and inst.components.skilltreeupdater:IsActivated("weerclops_smash_success") then
		inst.structure_destroyed_tick = inst.structure_destroyed_tick + 1
		if inst.structure_destroyed_tick >= 5 then
			if (data.target:HasTag("wall") or data.target:HasTag("structure")) and not (data.target:HasTag("sign") or data.target:HasTag("faced_chair")) then
				inst.components.sanity:DoDelta(50)
				inst.structure_destroyed_tick = 0
			end
		end
	end
end
--------------------------------------------------------------------------
local function OnSave(inst, data)
	if inst.stage == 1 then data.stage = 1 end
	if inst.stage == 2 then data.stage = 2 end
	if inst.stage == 3 then data.stage = 3 end
	if inst.stage == 4 then data.stage = 4 end
end

local function OnLoad(inst, data)
	if data.stage == nil then onbecamedeerclops(inst) end
		if data.stage == 1 then
			inst.components.sanity.dapperness = -TUNING.DAPPERNESS_TINY
			inst.stage = 1
		end
		if data.stage == 2 then
			inst.components.sanity.dapperness = -TUNING.DAPPERNESS_LARGE/2
			inst.stage = 2
		end
		if data.stage == 3 then
			inst.components.sanity.dapperness = -TUNING.DAPPERNESS_LARGE
			inst.stage = 3
		end
		if data.stage == 4 then
			inst.components.sanity.dapperness = -TUNING.DAPPERNESS_LARGE*3
			inst.stage = 4
		end
end
--------------------------------------------------------------------------
local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon( "weerclops.tex" )

	inst:AddTag("weerclops")

    inst:AddTag("HASHEATER") -- from heater component

	if TheNet:GetServerGameMode() == "quagmire" then
		inst.regorged = true
		inst:AddTag("fridge")-- 50% slower spoilage
		inst:AddTag("nocool")
	else
		inst:ListenForEvent("setowner", OnSetOwner)
	end
end

local master_postinit = function(inst)
	inst.starting_inventory = start_inv[TheNet:GetServerGameMode()] or start_inv.default

	inst.soundsname = "weerclops"
	inst.customidleanim = "idle_weerclops"

	inst.components.health:SetMaxHealth(TUNING.WEERCLOPS_HEALTH)
	inst.components.hunger:SetMax(TUNING.WEERCLOPS_HUNGER)
	inst.components.sanity:SetMax(TUNING.WEERCLOPS_SANITY)

    inst.components.sanity:AddSanityAuraImmunity("deerclops")

	inst.components.combat.customdamagemultfn = CustomCombatDamage

    inst:AddComponent("preserver")
    inst.components.preserver:SetPerishRateMultiplier(SkillBasedPreserverRateFn)

    inst:AddComponent("heater")
    inst.components.heater:SetThermics(false, false)
    inst.components.heater.heat = -20

	inst.components.foodaffinity:AddPrefabAffinity("watermelonicle", TUNING.AFFINITY_15_CALORIES_SMALL)

	inst.structure_destroyed_tick = 0

	inst.freezepower = TUNING.WEERCLOPS_FREEZE_POWER

	inst.freezetimer = 3

	inst.hasiceaura = false

	inst.iceauraenabled = false

	inst.components.workmultiplier:AddMultiplier(ACTIONS.HAMMER, 1.5, inst)

	inst.components.temperature:SetFreezingHurtRate(TUNING.WILSON_HEALTH / TUNING.WEERCLOPS_FREEZING_KILL_TIME)-- freezing dmg reduction
	inst.components.temperature:SetOverheatHurtRate(TUNING.WILSON_HEALTH / TUNING.WEERCLOPS_OVERHEAT_KILL_TIME)-- overheating dmg addition

	inst.components.health.fire_damage_scale = 2

	if inst.components.slipperyfeet ~= nil then
		inst.components.slipperyfeet.threshold = 2400
	end

	inst:WatchWorldState("season", OnWinter)
	OnWinter(inst, TheWorld.state.season)

	if TheNet:GetServerGameMode() == "lavaarena" then
		return
	end

	inst.OnSave = OnSave
	inst.OnLoad = OnLoad
	inst.OnNewSpawn = onbecamedeerclops

	inst:ListenForEvent("onhitother", weerclops_onhit)
	inst:ListenForEvent("ms_respawnedfromghost", OnRespawnedFromGhost)
	inst:ListenForEvent("timerdone", OnTimerDone)
	inst:ListenForEvent("onattackother", onattack)
	inst:ListenForEvent("attacked", OnAttacked)
	inst:ListenForEvent("finishedwork", structuresated)
	inst:ListenForEvent("killed", onkilled)
	inst:ListenForEvent("temperaturedelta", OnTemperatureDelta)

end
--------------------------------------------------------------------------
local MAXRANGE = 3
local NO_TAGS_NO_PLAYERS =	{ "INLIMBO", "notarget", "noattack", "invisible", "wall", "player", "companion" }
local NO_TAGS =				{ "INLIMBO", "notarget", "noattack", "invisible", "playerghost" }
local COMBAT_TARGET_TAGS = { "_combat" }

local function OnUpdateIce(inst)
    inst.range = inst.range + .75

    local x, y, z = inst.Transform:GetWorldPosition()
    for i, v in ipairs(TheSim:FindEntities(x, y, z, inst.range + 3, COMBAT_TARGET_TAGS, inst.canhitplayers and NO_TAGS or NO_TAGS_NO_PLAYERS)) do
        if not inst.ignore[v] and
            v:IsValid() and
            v.entity:IsVisible() and
            v.components.combat ~= nil then
            local range = inst.range + v:GetPhysicsRadius(0)
            if v:GetDistanceSqToPoint(x, y, z) < range * range then
                if inst.owner ~= nil and not inst.owner:IsValid() then
                    inst.owner = nil
                end
                if inst.owner ~= nil then
					if inst.owner.components.combat ~= nil and
						inst.owner.components.combat:CanTarget(v) and
						not inst.owner.components.combat:IsAlly(v)
					then
                        inst.ignore[v] = true
						v.components.combat:GetAttacked(v.components.follower and v.components.follower:GetLeader() == inst.owner and inst or inst.owner, inst.damage, nil, nil)
                    end
                end
            end
        end
    end

    if inst.range >= MAXRANGE then
        inst.components.updatelooper:RemoveOnUpdateFn(OnUpdateIce)
    end
end

local function SetFXOwner(inst, owner)
    inst.owner = owner
    inst.canhitplayers = not owner:HasTag("player") or TheNet:GetPVPEnabled()
    inst.ignore[owner] = true
end

local function MakeFX(name, anim, damage)
    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddNetwork()

        inst:AddTag("FX")

        inst.Transform:SetFourFaced()

        inst.AnimState:SetBank("deerclops")
        inst.AnimState:SetBuild("deerclops_mutated")
        inst.AnimState:PlayAnimation("ice_impact")

        inst.Transform:SetScale(0.7, 0.7, 0.7)

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        inst:AddComponent("updatelooper")
        inst.components.updatelooper:AddOnUpdateFn(OnUpdateIce)

        inst:ListenForEvent("animover", inst.Remove)
        inst.persists = false
        inst.damage = TUNING[damage]
        inst.range = .75
        inst.ignore = {}
        inst.canhitplayers = true

        inst.SetFXOwner = SetFXOwner

        return inst
    end

    return Prefab(name, fn, assets)
end

return MakePlayerCharacter("weerclops", prefabs, assets, common_postinit, master_postinit),
		MakePlacer("ice_rock_placer", "ice_boulder", "ice_boulder", "full"),
		MakeFX("weerclops_shatterfx", "idle", "ARMORBRAMBLE_DMG")