local assets =
{
    Asset("ANIM", "anim/deerclops_laser_hit_sparks_fx.zip"),
}

local prefabs =
{
    "deerclops_laserscorch",
    "deerclops_lasertrail",
    "deerclops_laserhit",
}

local LAUNCH_SPEED = .2
local RADIUS = .7

local function SetLightRadius(inst, radius)
    inst.Light:SetRadius(radius)
end

local function DisableLight(inst)
    inst.Light:Enable(false)
end

-- I'm creating Weerclops' laser as a semi-copy of deerclops_laser as its own prefab for the ease of it

local DAMAGE_CANT_TAGS = { "playerghost", "INLIMBO", "DECOR", "INLIMBO" }
local DAMAGE_ONEOF_TAGS = { "_combat", "NPC_workable", "CHOP_workable", "HAMMER_workable", "MINE_workable" }
local LAUNCH_MUST_TAGS = { "_inventoryitem" }
local LAUNCH_CANT_TAGS = { "locomotor", "INLIMBO" }

local function DoDamage(inst, targets, skiptoss)
    inst.task = nil

    local x, y, z = inst.Transform:GetWorldPosition()
    if inst.AnimState ~= nil then
        inst.AnimState:PlayAnimation("hit_"..tostring(math.random(5)))
        inst:Show()
        inst:DoTaskInTime(inst.AnimState:GetCurrentAnimationLength() + 2 * FRAMES, inst.Remove)

        inst.Light:Enable(true)
        inst:DoTaskInTime(4 * FRAMES, SetLightRadius, .5)
        inst:DoTaskInTime(5 * FRAMES, DisableLight)

        SpawnPrefab("deerclops_laserscorch").Transform:SetPosition(x, 0, z)
        local fx = SpawnPrefab("deerclops_lasertrail")
        fx.Transform:SetPosition(x, 0, z)
        fx:FastForward(GetRandomMinMax(.3, .7))
    else
        inst:DoTaskInTime(2 * FRAMES, inst.Remove)
    end

    inst.components.combat.ignorehitrange = true
    for i, v in ipairs(TheSim:FindEntities(x, 0, z, RADIUS + 3, nil, DAMAGE_CANT_TAGS, DAMAGE_ONEOF_TAGS)) do
        if not targets[v] and v:IsValid() and not v:IsInLimbo() and not (v.components.health ~= nil and v.components.health:IsDead()) then
            local vradius = v:GetPhysicsRadius(.5)
            local range = RADIUS + vradius
            if v:GetDistanceSqToPoint(x, y, z) < range * range then
                local isworkable = false
                if v.components.workable ~= nil then
                    local work_action = v.components.workable:GetWorkAction()
                    isworkable =
                        (   work_action == nil and v:HasTag("NPC_workable") ) or
                        (   v.components.workable:CanBeWorked() and
                            (   work_action == ACTIONS.CHOP or
                                work_action == ACTIONS.HAMMER or
                                work_action == ACTIONS.MINE
                            )
                        )
                end
                if isworkable then
                    targets[v] = true
					v.components.workable:WorkedBy(inst, 3)
                elseif inst.components.combat:CanTarget(v) then
                    targets[v] = true
                    if inst.caster ~= nil and inst.caster:IsValid() then
                        inst.caster.components.combat.ignorehitrange = true
						local wasfrozen = v.components.freezable ~= nil and v.components.freezable:IsFrozen()
						local frozenskill_1 = inst.caster.components.skilltreeupdater ~= nil and inst.caster.components.skilltreeupdater:IsActivated("weerclops_shatter_1")
						if wasfrozen and frozenskill_1 then
							v.components.combat:GetAttacked(inst.caster, TUNING.DEERCLOPS_DAMAGE*2, nil)
						else
							v.components.combat:GetAttacked(inst.caster, TUNING.DEERCLOPS_DAMAGE, nil)
						end
                        inst.caster.components.combat.ignorehitrange = false
                    else
                        inst.components.combat:DoAttack(v)
                    end
                    if v:IsValid() then
                        SpawnPrefab("deerclops_laserhit"):SetTarget(v)
                        if not v.components.health:IsDead() then
                            if v.components.freezable ~= nil then
                                if v.components.freezable:IsFrozen() then
                                    v.components.freezable:Unfreeze()
                                elseif v.components.freezable.coldness > 0 then
                                    v.components.freezable:AddColdness(-2)
                                end
                            end
                        end
                    end
                end
            end
        end
    end
    inst.components.combat.ignorehitrange = false
    for i, v in ipairs(TheSim:FindEntities(x, 0, z, RADIUS + 3, LAUNCH_MUST_TAGS, LAUNCH_CANT_TAGS)) do
        if not skiptoss[v] then
            local range = RADIUS + v:GetPhysicsRadius(.5)
            if v:GetDistanceSqToPoint(x, y, z) < range * range then
                if v.components.mine ~= nil then
                    targets[v] = true
                    skiptoss[v] = true
                    v.components.mine:Deactivate()
                end
                if not v.components.inventoryitem.nobounce and v.Physics ~= nil and v.Physics:IsActive() then
                    targets[v] = true
                    skiptoss[v] = true
                    Launch(v, inst, LAUNCH_SPEED)
                end
            end
        end
    end
end

local function Trigger(inst, delay, targets, skiptoss)
    if inst.task ~= nil then
        inst.task:Cancel()
        if (delay or 0) > 0 then
            inst.task = inst:DoTaskInTime(delay, DoDamage, targets or {}, skiptoss or {})
        else
            DoDamage(inst, targets or {}, skiptoss or {})
        end
    end
end

local function KeepTargetFn()
    return false
end

local function common_fn(isempty)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddNetwork()

    if not isempty then
        inst.entity:AddAnimState()
        inst.AnimState:SetBank("deerclops_laser_hits_sparks")
        inst.AnimState:SetBuild("deerclops_laser_hit_sparks_fx")
        inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
        inst.AnimState:SetLightOverride(1)

        inst.entity:AddLight()
        inst.Light:SetIntensity(.5)
        inst.Light:SetRadius(.8)
        inst.Light:SetFalloff(.5)
        inst.Light:SetColour(1, .2, .3)
        inst.Light:Enable(false)
    end

    inst:Hide()

    inst:AddTag("notarget")
    inst:AddTag("hostile")

    inst:SetPrefabNameOverride("deerclops")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(TUNING.DEERCLOPS_DAMAGE)
    inst.components.combat:SetKeepTargetFunction(KeepTargetFn)

    inst.task = inst:DoTaskInTime(0, inst.Remove)
    inst.Trigger = Trigger
    inst.persists = false

    return inst
end

local function fn()
    return common_fn(false)
end

local function emptyfn()
    return common_fn(true)
end

return Prefab("weerclops_laser", fn, assets, prefabs),
    Prefab("weerclops_laserempty", emptyfn, assets, prefabs)
