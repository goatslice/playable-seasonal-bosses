local UI_LEFT, UI_RIGHT = -214, 228
local UI_VERTICAL_MIDDLE = (UI_LEFT + UI_RIGHT) * 0.5
local UI_TOP, UI_BOTTOM = 176, 20

--------------------------------------------------------------------------------------------------

local WEERCLOPS_SKILL_STRINGS = STRINGS.SKILLTREE.WEERCLOPS

--------------------------------------------------------------------------------------------------

local function CreateAddTagFn(tag)
    return function(inst) inst:AddTag(tag) end
end

local function CreateRemoveTagFn(tag)
    return function(inst) inst:RemoveTag(tag) end
end

local function CreateAccomplishmentLockFn(key)
    return
        function(prefabname, activatedskills, readonly)
            return readonly and "question" or TheGenericKV:GetKV(key) == "1"
        end
end

--------------------------------------------------------------------------------------------------

local ORDERS =
{
    { "pupil",    { UI_LEFT, UI_TOP } },
    { "ice",   { UI_LEFT, UI_TOP } },
    { "destroy",    { UI_LEFT, UI_TOP } },
    { "allegiance1", { UI_LEFT, UI_TOP } },
    { "allegiance2", { UI_LEFT, UI_TOP } },
}

--------------------------------------------------------------------------------------------------

local function BuildSkillsData(SkillTreeFns)
    local skills =
    {
		-- Become a Deerclops' pupil; learn to 1-hit structures when using your claws to destroy
		weerclops_pupil = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_PUPIL_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_PUPIL_DESC,
			icon = "weerclops_pupil",
			pos = {3, 125},
			group = "pupil",
			tags = {},
			root = true,
			defaultfocus = true,
            connects = {
                "weerclops_fridge_1",
                "weerclops_ice_craft_1",
                "weerclops_cold_aura_1",
                "weerclops_cryofreeze_1",
                "weerclops_smash_success",
            }
		},

        --------------------------------------------------------------------------

		-- Food held by Deerclops will spoil 20% slower
		weerclops_fridge_1 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_FRIDGE_1_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_FRIDGE_1_DESC,
			icon = "weerclops_fridge_1",
			pos = {-30, 177},
			group = "ice",
			tags = {"ice"},
            connects = {
                "weerclops_fridge_2",
            },
		},

		-- Food held by Deerclops will spoil 40% slower
		weerclops_fridge_2 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_FRIDGE_2_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_FRIDGE_2_DESC,
			icon = "weerclops_fridge_2",
			pos = {-70, 187},
			group = "ice",
			tags = {"ice"},
            connects = {
                "weerclops_fridge_3",
            },
		},

		-- Food held by Deerclops will spoil 60% slower
		weerclops_fridge_3 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_FRIDGE_3_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_FRIDGE_3_DESC,
			icon = "weerclops_fridge_3",
			pos = {-110, 197},
			group = "ice",
			tags = {"ice"},
		},

        --------------------------------------------------------------------------

		-- Learn to craft a bit of ice, at the cost of health
		weerclops_ice_craft_1 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_ICE_CRAFT_1_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_ICE_CRAFT_1_DESC,
			icon = "weerclops_ice_craft_1",
			pos = {31, 178},
			group = "ice",
			tags = {"ice"},
            connects = {
                "weerclops_ice_craft_2",
            },
		},

		-- Learn to craft Glaciers, at the cost of ice
		weerclops_ice_craft_2 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_ICE_CRAFT_2_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_ICE_CRAFT_2_DESC,
			icon = "weerclops_ice_craft_2",
			pos = {69, 207},
			group = "ice",
			tags = {"ice"},
		},

        --------------------------------------------------------------------------

		-- When at lower temps, gain a chilling aura around you
		weerclops_cold_aura_1 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_COLD_AURA_1_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_COLD_AURA_1_DESC,
			icon = "weerclops_cold_aura_1",
			pos = {81, 127},
			group = "ice",
			tags = {"ice"},
            onactivate = function(inst)
                inst.hasiceaura = true
			end,
            ondeactivate = function(inst)
                inst.hasiceaura = false
            end,
            connects = {
                "weerclops_slowing_1",
                "weerclops_cold_aura_2",
            },
		},

		-- Your chilling aura will allow you to have your winter aoe coldness year-round
		weerclops_cold_aura_2 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_COLD_AURA_2_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_COLD_AURA_2_DESC,
			icon = "weerclops_cold_aura_2",
			pos = {95, 169},
			group = "ice",
			tags = {"ice"},
		},

		-- Enemies recently frozen by Deerclops will have a short slowing debuff
		weerclops_slowing_1 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_SLOWING_1_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_SLOWING_1_DESC,
			icon = "weerclops_slowing_1",
			pos = {126, 127},
			group = "ice",
			tags = {"ice"},
            connects = {
                "weerclops_slowing_2",
            },
		},

		-- Increase time slowed from freezing a mob
		weerclops_slowing_2 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_SLOWING_2_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_SLOWING_2_DESC,
			icon = "weerclops_slowing_2",
			pos = {142, 169},
			group = "ice",
			tags = {"ice"},
		},

        --------------------------------------------------------------------------

		-- Encase yourself within ice, gain a small health regen while immobile
		weerclops_cryofreeze_1 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_CRYOFREEZE_1_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_CRYOFREEZE_1_DESC,
			icon = "weerclops_cryofreeze_1",
			pos = {-80, 135},
			group = "ice",
			tags = {"ice"},
            connects = {
                "weerclops_cryofreeze_2",
            },
		},

		-- Increase your cryofreeze's health regen, increase its time
		weerclops_cryofreeze_2 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_CRYOFREEZE_2_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_CRYOFREEZE_2_DESC,
			icon = "weerclops_cryofreeze_2",
			pos = {-120, 115},
			group = "ice",
			tags = {"ice"},
		},

        --------------------------------------------------------------------------

		-- Defeat a Deerclops to unlock
        weerclops_deerclops_lock = {
            group = "ice",
			tags = {"lock"},
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_DEERCLOPS_LOCK_DESC,
			pos = {-39, 91},
			root = true,
            connects = { "weerclops_shatter_1" },

            lock_open = CreateAccomplishmentLockFn("weerclops_superior_deerclops"),
        },

		-- Deal double damage to a creature if it's frozen
		weerclops_shatter_1 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_SHATTER_1_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_SHATTER_1_DESC,
			icon = "weerclops_shatter_1",
			pos = {-73, 80},
			group = "ice",
			tags = {"ice"},
            connects = {
                "weerclops_shatter_2",
            },
		},

		-- Killing a frozen creature will shatter, causing aoe damage and coldness around it
		weerclops_shatter_2 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_SHATTER_2_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_SHATTER_2_DESC,
			icon = "weerclops_shatter_2",
			pos = {-109, 60},
			group = "ice",
			tags = {"ice"},
            connects = {
                "weerclops_shatter_3",
            },
		},

		-- Double the damage and range of shattering a frozen enemy
		weerclops_shatter_3 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_SHATTER_3_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_SHATTER_3_DESC,
			icon = "weerclops_shatter_3",
			pos = {-146, 40},
			group = "ice",
			tags = {"ice"},
		},

        --------------------------------------------------------------------------

		-- Experience a Winter to unlock
        weerclops_winter_lock = {
            group = "ice",
			tags = {"lock"},
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_WINTER_LOCK_DESC,
			pos = {41, 91},
			root = true,
            connects = { "weerclops_coldness_1" },

            lock_open = CreateAccomplishmentLockFn("weerclops_winter"),
        },

		-- Double the coldness you deal when attacking, increases your aoe winter coldness
		weerclops_coldness_1 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_COLDNESS_1_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_COLDNESS_1_DESC,
			icon = "weerclops_coldness_1",
			pos = {74, 80},
			group = "ice",
			tags = {"ice"},
            onactivate = function(inst)
                inst.freezepower = TUNING.WEERCLOPS_FREEZE_POWER*2
			end,
            ondeactivate = function(inst)
                inst.freezepower = TUNING.WEERCLOPS_FREEZE_POWER
            end,
            connects = {
                "weerclops_coldness_2",
            },
		},

		-- Double the time your target stays frozen
		weerclops_coldness_2 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_COLDNESS_2_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_COLDNESS_2_DESC,
			icon = "weerclops_coldness_2",
			pos = {110, 60},
			group = "ice",
			tags = {"ice"},
            onactivate = function(inst)
                inst.freezetimer = 6
			end,
            ondeactivate = function(inst)
                inst.freezetimer = 3
            end,
            connects = {
                "weerclops_coldness_3",
            },
		},

		-- Enemies that attack you will have coldness added to them 
		weerclops_coldness_3 = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_COLDNESS_3_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_COLDNESS_3_DESC,
			icon = "weerclops_coldness_3",
			pos = {147, 40},
			group = "ice",
			tags = {"ice"},
		},

        --------------------------------------------------------------------------

		-- Destroying 5 structures in a row will grant a massive sanity boost
		weerclops_smash_success = {
			title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_SMASH_SUCCESS_TITLE,
			desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_SMASH_SUCCESS_DESC,
			icon = "weerclops_smash_success",
			pos = {2, 51},
			group = "destroy",
			tags = {"destroy"},
		},

        --------------------------------------------------------------------------

		-- Shadow
        weerclops_allegiance_count_lock_1 = {
            desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_ALLEGIANCE_LOCK_1_DESC,
            pos = {UI_LEFT + 41, UI_BOTTOM + 86},
            group = "allegiance1",
            tags = {"allegiance","lock"},
            root = true,
            lock_open = function(prefabname, activatedskills, readonly)
                return SkillTreeFns.CountSkills(prefabname, activatedskills) >= 12
            end,
        },

		-- Lunar
        weerclops_allegiance_count_lock_2 = {
            desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_ALLEGIANCE_LOCK_1_DESC,
            pos = {UI_RIGHT - 52, UI_BOTTOM + 86},
            group = "allegiance1",
            tags = {"allegiance","lock"},
            root = true,
            lock_open = function(prefabname, activatedskills, readonly)
                return SkillTreeFns.CountSkills(prefabname, activatedskills) >= 12
            end,
        },

        weerclops_allegiance_lock_2 = SkillTreeFns.MakeFuelWeaverLock(
            { pos = {UI_LEFT + 41, UI_BOTTOM + 50}, 
			  group = "allegiance1",
			}
        ),

        weerclops_allegiance_lock_4 = SkillTreeFns.MakeNoLunarLock(
            { pos = {UI_LEFT + 41, UI_BOTTOM + 126},
			  group = "allegiance1",
			}
        ),

        weerclops_allegiance_shadow = {
            title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_ALLEGIANCE_SHADOW_TITLE,
            desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_ALLEGIANCE_SHADOW_DESC,
            icon = "weerclops_allegiance_shadow",
            pos = {UI_LEFT + 8, UI_BOTTOM + 85},
            group = "allegiance1",
            tags = {"allegiance","shadow","shadow_favor"},
            locks = {"weerclops_allegiance_count_lock_1", "weerclops_allegiance_lock_2", "weerclops_allegiance_lock_4"},

            onactivate = function(inst, fromload)
                inst:AddTag("player_shadow_aligned")

                local damagetyperesist = inst.components.damagetyperesist
                if damagetyperesist then
                    damagetyperesist:AddResist("shadow_aligned", inst, TUNING.SKILLS.WILSON_ALLEGIANCE_SHADOW_RESIST, "allegiance_shadow")
                end
                local damagetypebonus = inst.components.damagetypebonus
                if damagetypebonus then
                    damagetypebonus:AddBonus("lunar_aligned", inst, TUNING.SKILLS.WILSON_ALLEGIANCE_VS_LUNAR_BONUS, "allegiance_shadow")
                end
            end,
            ondeactivate = function(inst, fromload)
                inst:RemoveTag("player_shadow_aligned")

                local damagetyperesist = inst.components.damagetyperesist
                if damagetyperesist then
                    damagetyperesist:RemoveResist("shadow_aligned", inst, "allegiance_shadow")
                end
                local damagetypebonus = inst.components.damagetypebonus
                if damagetypebonus then
                    damagetypebonus:RemoveBonus("lunar_aligned", inst, "allegiance_shadow")
                end
            end,
        },

        weerclops_allegiance_lock_3 = SkillTreeFns.MakeCelestialChampionLock(
            { pos = {UI_RIGHT - 52, UI_BOTTOM + 50}, 
			  group = "allegiance2",
			}
        ),

        weerclops_allegiance_lock_5 = SkillTreeFns.MakeNoShadowLock(
            { pos = {UI_RIGHT - 52, UI_BOTTOM + 126},
			  group = "allegiance2",
			}
        ),

        weerclops_allegiance_lunar = {
		    title = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_ALLEGIANCE_LUNAR_TITLE,
            desc = WEERCLOPS_SKILL_STRINGS.WEERCLOPS_ALLEGIANCE_LUNAR_DESC,
            icon = "weerclops_allegiance_lunar",
            pos = {UI_RIGHT - 20, UI_BOTTOM + 85},
            group = "allegiance2",
            tags = {"allegiance","lunar","lunar_favor"},
            locks = {"weerclops_allegiance_count_lock_2", "weerclops_allegiance_lock_3", "weerclops_allegiance_lock_5"},

            onactivate = function(inst, fromload)
                inst:AddTag("player_lunar_aligned")

                local damagetyperesist = inst.components.damagetyperesist
                if damagetyperesist then
                    damagetyperesist:AddResist("lunar_aligned", inst, TUNING.SKILLS.WILSON_ALLEGIANCE_LUNAR_RESIST, "allegiance_lunar")
                end
                local damagetypebonus = inst.components.damagetypebonus
                if damagetypebonus then
                    damagetypebonus:AddBonus("shadow_aligned", inst, TUNING.SKILLS.WILSON_ALLEGIANCE_VS_SHADOW_BONUS, "allegiance_lunar")
                end
            end,
            ondeactivate = function(inst, fromload)
                inst:RemoveTag("player_lunar_aligned")

                local damagetyperesist = inst.components.damagetyperesist
                if damagetyperesist then
                    damagetyperesist:RemoveResist("lunar_aligned", inst, "allegiance_lunar")
                end
                local damagetypebonus = inst.components.damagetypebonus
                if damagetypebonus then
                    damagetypebonus:RemoveBonus("shadow_aligned", inst, "allegiance_lunar")
                end
            end,
        },
	}

    return {
        SKILLS = skills,
        ORDERS = ORDERS,
    }
end

--------------------------------------------------------------------------------------------------

return BuildSkillsData